@extends('layouts.master')


@section('content')
<div class="container">
   <hr>	
	<h3 class="text-center">Departments</h3>	
	<a href="{{ route('department.create') }}" class="btn btn-success">Create</a>
	<hr>
	<table class= "table table-hover">
		<thead>
            <th>Department name</th>
            <th>Action</th>			
										
		</thead>		
		<tbody>
			@if($departments->count() > 0)
				@foreach($departments as $department)
					<tr>
						<td>
							<a href="{{ route('department.show', ['slug' => $department->slug ]) }}">{{ $department->name }}</a>
						</td>
					
						<td>
							<form action="{{ route('department.destroy', ['id' => $department->id ]) }}" method="POST">
								{{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <a href="{{ route('department.show', ['slug' => $department->slug ]) }}" class="btn btn-info">Show</a>
                                <a href="{{ route('department.edit', ['id' => $department->id ]) }}" class="btn btn-primary">Edit</a>
                                <button class="btn btn-xs btn-danger">Delete</button>
							</form>
						</td>
					</tr>
				@endforeach
			@else
				<tr> 
					<th colspan="5" class="text-center">No Departments yet</th>
				</tr>
			@endif		
		</tbody>	
	</table>
</div>		
@endsection