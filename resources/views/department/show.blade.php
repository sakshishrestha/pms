@extends('layouts.master')


@section('content')
<br>

<div class="container">
    <hr>
		<h3 class="page-header">Department: {{ $department->name }}</h3>
		<a href="{{route('department.index')}}" class="btn btn-success">Back</a>
		<hr>

		<table class= "table table-hover">
		<thead>
			<th>Role</th>
			<th>Salary</th>
		</thead>
		
		<tbody>
			@if($department->roles->count() > 0)
				@foreach($department->roles as $role)
					<tr>
						<td>
							<a href="{{ route('role.show', ['slug'=>$role->slug])}}">{{ $role->name }}</a>
						</td>
						<td>{{ $role->salary }}</td>
					</tr>
				@endforeach
			@else
				<tr> 
					<th colspan="5" class="text-center">No Roles assigned in this department yet</th>
				</tr>
			@endif
		
		</tbody>
	
	</table>
</div>		
@endsection