@extends('layouts.master')

@section('content')  

<div class="container">
<form method="post" action="{{route('department.update', $department->id)}}" name="form1" enctype="multipart/form-data" ><br/>
    @method('PATCH')
    {{csrf_field()}}
    <div class="float-left"><h4>Edit Department</h4></div>
    <div class="float-right"><a href="{{route('department.index')}}" class="btn btn-success">Back</a></div> <br>
    <hr/>
    <div class="form-group">
        <label>Department Name</label>
        <input type="name" class="form-control" name="name" value="{{ $department->name }}">
        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
    </div>
    <button type="submit" class="btn btn-primary">Update</button>

    
        
</form>
</div>
    
@endsection

