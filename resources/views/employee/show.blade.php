@extends('layouts.master')


@section('content')

<div class="container">
	<hr>
    <div class="float-left"><h4>Employee: {{ $employee->name }}</h4></div>
    <div class="float-right">
        <a href="{{route('employee.index')}}" class="btn btn-success">Back</a>
    </div> <br>
    <hr/>
       
	
	
	@auth
		<a href="{{ route('employee.edit',['id'=>$employee->id]) }}" class="btn btn-primary">Edit</a>	
		
	@endauth
	
	<br>
	<br>
	
	<table class="table table-hover" >
        <tr>
            <th>Emp ID:</th>
            <td>{{ $employee->id }}</td>		
        </tr>
		<tr>
			<th>Name:</th>
			<td>{{ $employee->name }}</td>		
		</tr>
		<tr>
			<th>Email</th>
			<td>{{ $employee->email }}</td>
		</tr>		
		<tr>
			<th>Department</th>
			<td>{{ $employee->role->department->name }}</td>
		</tr>										
		<tr>
			<th>Role</th>
			<td>{{ $employee->role->name }}</td>
		</tr>	
		<tr>
			<th> Basic Salary</th>
			<td> Rs. {{ $employee->role->salary }}</td>			
		</tr>			
		<tr>
			<th>Address</th>
			<td>{{ $employee->address }}</td>			
		</tr>
		<tr>
			<th>Number</th>
			<td>{{ $employee->contactnumber }}</td>			
		</tr>
		<tr>
			<th>Gender</th>
			<td>{{ $employee->gender }}</td>			
		</tr>
				
    </table>
</div>		
@endsection


