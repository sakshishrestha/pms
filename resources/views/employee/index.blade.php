@extends('layouts.master')

@section('content')

<div class="container">
    <hr>	
    <h3 class="text-center">Employees Details</h3>	
    <hr> 
    <div class="float-left">
        <a href="{{route('employee.create')}}" class="btn btn-success">Create</a>
    </div>
    
    <div class="float-right">
        <a href="{{route('employee.index')}}" class="btn btn-success">Back</a>
    </div><br><br>


    {{-- search bar--}}
    <form action="/search" method="post" role="search">
        {{ csrf_field() }}
        <div class="input-group">
            <input type="text" class="form-control" name="q"
                placeholder="Search users"> <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                    <span>Search</span>
                </button>
            </span>
        </div> 
    </form>
    @if(isset($details))
     <table class="table table-hover">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Contact</th>
            <th>Gender</th>
            <th>Role</th>
            <th>Action</th>
        </tr>
       
        @if($details->count()>0)
        @foreach ( $details as $employee)
        <tr>
            <td>{{ $employee->id }}</td>
           
            <td>{{ $employee->name }}</td>
            <td>{{ $employee->address }}</td>
            <td>{{ $employee->email }}</td>
            <td>{{ $employee->contactnumber }}</td>
            <td>{{ $employee->gender }}</td>
            <td>{{ $employee->role->name }}</td>
            <td>
                <form action="{{ route('employee.destroy',$employee->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('employee.show',$employee->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('employee.edit',$employee->id) }}">Edit</a>
                    <a href="{{ route('payrolls.show', ['id' => $employee->id]) }}" class="btn btn-info">Payroll</a>

                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        
        @endforeach
        @else
        <tr>
            <th colspan="5" class="text-center">Empty</th>
        </tr>
        @endif
    </table>
    {{-- <div class="text-center">{{ $details->links() }}</div> --}}
</div>
@endif
@endsection