@extends('layouts.master')

@section('content')  

<div class="container">
<form method="post" action="{{route('employee.update', $employee->id)}}" name="form1" enctype="multipart/form-data" ><br/>
    @method('PATCH')
    {{csrf_field()}}
    <div class="float-left"><h4>Add Employees</h4></div>
    <div class="float-right"><a href="{{route('employee.index')}}" class="btn btn-success">Back</a></div> <br>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Employee Name</label>
                <input type="name" class="form-control" name="name" value="{{ $employee->name }}">
                {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
            </div>
            <div class="form-group">
                <label>Address</label>
            <input type="text" class="form-control" name="address" value="{{ $employee->address }}">
            </div>
            <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="{{ $employee->email }}">
                </div>
            <div class="form-group">
                <label>Contact Number</label>
                <input type="integer" class="form-control" name="contactnumber" value="{{ $employee->contactnumber }}">
            </div>
        </div>
        <div class="col-md-6">
            <label>Gender</label>
            <select class="form-control" name="gender" value="{{ $employee->gender }}">
                <option>Male</option>
                <option>Female</option>
                <option>Others</option>
            </select>

            <div class="form-group">
                <label for="role">Select a Role</label>
                <select name="role_id"  cols="5" rows="5" class="form-control">
                    @foreach($roles as $role)
                        <option value="{{ $role->id}}">{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="form-group">
                <label for="full_time">Position:</label>
                <select name="full_time" id="full_time" class="form-control">
                    <option value="1">Full-Time</option>
                    <option value="0">Part-Time</option>					
                </select>
            </div>

            
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    
        
</form>
</div>
</div>
    
@endsection

