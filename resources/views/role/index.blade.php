@extends('layouts.master')


@section('content')

<div class="container">
    <hr>	
    <h3 class="text-center">Roles Info</h3>	
    <hr> 
    <div class="float-left">
    <a href="{{route('role.create')}}" class="btn btn-success">Create</a>
    </div><br><br>
    <hr>		
	<table class= "table table-hover">
		<thead>		
			<th>Name</th>
			<th>Department</th>
			<th>Salary</th>
			<th>Action</th>
		</thead>	
			
		<tbody>
			@if($roles->count()> 0)
				@foreach($roles as $role)
					<tr>						
						<td><a href="{{ route('role.show', ['slug' => $role->slug]) }}" >{{ $role->name}}</a></td>						
						
						<td>{{ $role->department->name }}</td>
                        <td> Rs. {{ $role->salary }}</td>
                        
                        
							
						<td>
							<form action="{{ route('role.destroy', ['id' => $role->id]) }}" method="POST">
								{{csrf_field() }}
                                {{method_field('DELETE')}}
                                <a href="{{ route('role.show', ['slug' => $role->slug]) }}" class="btn btn-info" >Show</a>
                                <a href="{{ route('role.edit', ['id' => $role->id]) }}" class="btn btn-primary">Edit</a>
					            <button class="btn btn-danger">Delete</button>
							</form>
						</td>
					</tr>
				@endforeach
			@else
				<tr> 
					<th colspan="5" class="text-center">Empty</th>
				</tr>
			@endif
		</tbody>
	</table>
</div>
@endsection