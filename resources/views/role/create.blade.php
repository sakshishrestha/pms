@extends('layouts.master')

@section('content')  

<div class="container">
<form method="post" action="{{route('role.store')}}" name="form1" enctype="multipart/form-data" ><br/>
    {{csrf_field()}}
    <div class="float-left"><h4>Roles</h4></div>
    <div class="float-right"><a href="{{route('role.index')}}" class="btn btn-success">Back</a></div> <br>
    <hr/>
    <div class="form-group">
        <label> Name</label>
        <input type="name" class="form-control" name="name" placeholder="Name">
        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
    </div>

    <div class="form-group">
        <label for="salary">Salary</label>
        <input type="number" name="salary" class="form-control" placeholder="In RS">
    </div>
    
    <div class="form-group">
        <label for="department">Select a department</label>
        <select name="department_id"  cols="5" rows="5" class="form-control">
            @foreach($departments as $department)
                <option value="{{ $department->id}}">{{ $department->name }}</option>
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
    
@endsection

