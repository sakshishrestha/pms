@extends('layouts.master')

@section('content')

	{{-- <div class="col-lg-12">
		<h1 class="page-header">Payroll : {{ $employee->name }}	
			 <input type="text" id="filterInput" onkeyup="filterFunction()" placeholder="Search Employees...."--> 
		</h1>	
	</div>

	<a href="{{ route('payrolls.create', ['id'=>$employee->id]) }}" class="btn btn-primary">Create</a> --}}
<div class="container">
	<hr>	
	<h3 class="text-center">Payroll : {{ $employee->name}}</h3>	
			<hr> 
			<div class="float-left">
				<a href="{{route('payrolls.create',['id'=>$employee->id])}}" class="btn btn-success">Create</a>
			</div>
			<div class="float-right">
				<a href="{{route('employee.index')}}" class="btn btn-success">Back</a>

			</div><br><br>
			<hr>

	@if($employee->full_time)
		<p><b>Full-Time</b> :  Yes</p>
		<p><b>Base Salary</b>: {{ $employee->role->salary }}</p>
	@else
		<p><b>Part-Time</b> : Yes</p>
		<br>
		<p><b>Base Salary</b>: 0</p>
	@endif
		

	<br>

	<table class= "table table-hover" id="filterTable">
		<thead>	
			<th>Date-issued</td>
			<th>Travel</th>
			<th>Food</th>
			<th>Others</th>
			<th>Leave Days</th>
			<th>Status</th>
		
			<th>Gross</th>
			
			<th>Edit</th>	
			
		</thead>		
			
		<tbody>
			@if($employee->payrolls->count()> 0)
				@foreach($employee->payrolls as $payroll)
					<tr>		
						<td>{{ $payroll->created_at->toDateString() }}
						
						<td>{{ $payroll->travel }}</td>
						<td>{{ $payroll->food }}</td>
						<td>{{ $payroll->others }}</td>
						<td>{{ $payroll->attendence }}</td>
						<td>{{ $payroll->status }}</td>
						{{-- {{dd($payroll)}} --}}
						{{-- <td>{{ $payroll->tax }}</td> --}}
						<td> Rs. {{ $payroll->gross }}</td>
						
						<td>
							<a href="{{ route('payrolls.edit', ['id' => $payroll->id]) }}" class="btn btn-success">Edit</a>
						</td>
						<td>
							<form action="{{ route('payrolls.destroy', ['id' => $payroll->id]) }}" method="POST">
								{{csrf_field() }}
								{{method_field('DELETE')}}
								<button class="btn btn-danger">Delete</button>
							</form>
						</td>
					</tr>
				@endforeach
			@else
				<tr> 
					<th colspan="5" class="text-center">Empty</th>
				</tr>
			@endif
		</tbody>							
	</table>
</div>
@endsection