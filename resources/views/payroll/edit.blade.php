@extends('layouts.master')

@section('content')
<div class="container">
	<hr>
	<div class="col-lg-12">
		<h1 class="page-header">Edit Payroll : {{ $payroll->employee->name }}</h1>
	</div>
		@if($payroll->employee->full_time)
			<p><b>Full-Time</b> :  Yes</p>
			<p><b>Base Salary</b>: {{ $payroll->employee->role->salary }}</p>
		@else
			<p><b>Part-Time<b> : Yes</p>
			<br>
			<p><b>Base Salary<b>: 0</p>
		@endif
		<hr>
		<form action="{{ route('payrolls.update',['id'=>$payroll->id])}}" method="POST"
			class="form-horizontal">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
			
		
			
			<div class="form-group">
				<label class="control-label col-md-1" for="travel">Travel Allowance: </label>
				<div class="col-md-4">					
					<input type="number" name="travel" class="form-control" value="{{ $payroll->travel }}">		
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-1" for="food">Food Allowance: </label>
				<div class="col-md-4">
					<input type="number" name="food" class="form-control" value="{{ $payroll->food }}">	
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-1" for="Others">Others: </label>
				<div class="col-md-4">
					<input type="number" name="others" class="form-control" value="{{ $payroll->others }}">	
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-1" for="attendence">Leave Days: </label>
				<div class="col-md-4">
					<input type="number" name="attendence" class="form-control" value="{{ $payroll->attendence }}">	
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-1" for="status">Status: </label>
				<div class="col-md-4">
					<select type="text" name="status" class="form-control" value="{{ $payroll->status }}">
						<option>Paid</option>
						<option>UnPaid</option>
					</select>	
				</div>
			</div>
		
			
			<div class="col-lg-4 text-center">
				<button class="btn btn-success" type="submit" >Update</button>
			</div>
		</form> 
	</div>
@endsection
