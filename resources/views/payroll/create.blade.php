{{-- {{die('test')}} --}}

@extends('layouts.master')

@section('content')

	<div class="container">
		<hr>
		<h3 class="page-header">Payroll : {{ $employee->name }}</h3>
		
		<div class="float-right">
				<a href="{{route('employee.index')}}" class="btn btn-success">Back</a>
		</div>
		@if($employee->full_time)
			<p><b>Full-Time</b> :  Yes</p>
			<p><b>Base Salary</b>: {{ $employee->role->salary }}</p>
		@else
			<p><b>Part-Time<b> : Yes</p>
			<br>
			<p><b>Base Salary<b>: 0</p>
		@endif
		<hr>
		<form method="post" action="{{ route('payrolls.store',['id'=>$employee->id])}}" 
			class="form-horizontal">
				{{ csrf_field() }}
			
			<div class="form-group">
				<label class="control-label col-md-1" for="hours">Travel Allowance:</label>
				<div class="col-md-4">					
					<input type="number" name="travel" class="form-control">		
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-1" for="hours">Food Allowance: </label>
				<div class="col-md-4">					
					<input type="number" name="food" class="form-control">		
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-1" for="hours">Others: </label>
				<div class="col-md-4">					
					<input type="number" name="others" class="form-control">		
				</div>
			</div>
			
			
			<div class="form-group">
				<label class="control-label col-md-1" for="hours">Leave Days: </label>
				<div class="col-md-4">					
					<input type="number" name="attendence" class="form-control">		
				</div>
			</div>

			<div class="form-group col-md-4">
				<label>TAX</label>
					<select class="form-control">
						<option>10%</option>
						
					</select>
			</div>

			<div class="form-group col-md-4">
				<label>Status</label>
					<select class="form-control" name="status">
						<option>Paid</option>
						<option>Unpaid</option>
					</select>
			</div>
						
			
			<div class="col-lg-4 text-center">
				<button class="btn btn-success" type="submit" >Submit</button>
			</div>
		</form> 
	</div>
@endsection
