@extends('layouts.master')

@section('content')
<div class="container">
    
    <hr>	
        <h3 class="text-center">Dashboard</h3>	
    <hr>
        <div class="card">
            <div class="row">
                
                <div class="card col-lg-3 text-center card bg-light">
                   <div class="card-header">Payroll issued</div>
                    <div class="card-body">{{ $payrollsCount }}</div>		
                </div>
               
                
                <div class="card col-lg-3 text-center card bg-light">
                    <div class="card-header">Employee Count</div>
                    <div class="card-body">{{ $employeesCount }}</div>		
                </div>

                <div class="card col-lg-3 text-center card bg-light">
                    <div class="card-header">Role Count</div>
                    <div class="card-body">{{ $roles }}</div>		
                </div>

                <div class="card col-lg-3 text-center card bg-light">
                    <div class="card-header">Department</div>
                    <div class="card-body">{{ $departments }}</div>		
                </div>
            
            </div>
        </div>
        
    
        <div class="card">
            <div class="card-header">Latest Employee Details:</div>
                <table class= "table table-hover">
                    <thead>	
                        <tr>
                            <th>Date Added</td>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Department</th>
                        </tr>
                    </thead>		
                        
                    <tbody>
                        @if($employees->count()> 0)
                            @foreach($employees as $employee)
                                <tr>		
                                    <td>{{ $employee->created_at->toDateString() }}</td>
                                    <td>{{ $employee->name }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->role->name }}</td>
                                    <td>{{ $employee->role->department->name }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr> 
                                <th colspan="5" class="text-center">Empty</th>
                            </tr>
                        @endif
                    </tbody>							
                </table>
            </div>
        <hr>
        
        
        <div class="card">
            <div class="card-header">Latest issued Payroll:</div>
                <table class= "table table-hover">
                    <thead>	
                        <tr>
                            <th>Date-issued</td>
                            <th>Name</th>
                            <th>Salary</th>
                            <th>Travel</th>
                            <th>Food</th>
                            <th>Others</th>
                            <th>Leave</th>
                            {{-- <th>TAX</th> --}}
                            <th>Status</th>
                            <th>Gross</th>
                        </tr>
                    </thead>		
                        
                    <tbody>
                        @if($payrolls->count()> 0)
                            @foreach($payrolls as $payroll)
                                <tr>		
                                    <td>{{ $payroll->created_at->toDateString() }}</td>
                                    <td>{{ $payroll->employee->name }}</td>
                                    <td>{{ $payroll->employee->role->salary }}</td>
                                    <td>{{ $payroll->travel }}</td>
                                    <td>{{ $payroll->food }}</td>
                                    <td>{{ $payroll->others }}</td>
                                    <td>{{ $payroll->attendence }}</td>
                                    <td>{{ $payroll->status }}</td>
                                    {{-- <td>{{ $payroll->tax }}</td> --}}
                                    <td> Rs. {{ $payroll->gross }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr> 
                                <th colspan="5" class="text-center">Empty</th>
                            </tr>
                        @endif
                    </tbody>							
                </table>
            </div>

@endsection