<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payroll extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable=['employee_id','travel','food','others','status','attendence','total'];

    public function employee(){
        return $this->belongsTo('App\Employee');
    }
    public function grossPay(){
        $calc = 0;
        $calc1 = 0;
        if($this->employee->full_time){
            $calc = ($this->employee->role->salary / 30) * $this->attendence;
            $calc1 = $this->employee->role->salary * 0.10 ;
            return $this->gross = ($this->employee->role->salary + $this->travel + $this->food + $this->others) - ( $calc + $calc1 );
        }

        return $this->gross = 0;

    }
}
