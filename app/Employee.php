<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Employee extends Model
{
    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $dates=['deleted_at'];

    protected $fillable = [
        'name', 'slug','address','email','contactnumber','gender','role_id','full_time',
    ];
    public $with = ['role','payrolls'];

    public function role(){
        return $this->belongsTo('App\Role');

    }

    public function payrolls(){
        return $this->hasMany('App\Payroll');
    }
}


