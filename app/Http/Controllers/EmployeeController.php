<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Role;
use App\Department;
use App\Payroll;
use Session;
use paginate;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $emp = Employee::all();
        return view('employee.index', ['details'=>Employee::paginate(50)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $roles=Role::all();
        if($roles->count()==0){
            Session::flash('Success','you must have atleast 1 role created before attempting to create am employee');
            return redirect()->back;
        }
        return view('employee.create')->with('roles',$roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|max:255',
            'address'=>'required',
            'email'=>'required|email',
            'contactnumber'=>'required',
            'gender'=>'required',
            'role_id'=>'required',
            'full_time'=>'required|bool'


        ]);
        
        $employee = Employee::create([
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'email' => $request->email,
            'address' => $request->address,
            'contactnumber' => $request->contactnumber,
            'gender' => $request->gender,
            'role_id' => $request->role_id,
            'full_time' => $request->full_time,

        ]);

        $payroll = new Payroll;
        $payroll->employee_id = $employee->id;
        $payroll->save();
        $employee->save();

        $request->session()->flash('status','New Employee created');
        return redirect()->route('employee.index');

        
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('employee.show',['employee'=>Employee::findOrFail($id)]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('employee.edit',['employee'=>Employee::find($id),
        'roles'=>Role::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee=Employee::findOrFail($id);
        $this->validate($request,[
            'name'=>'required',
            'address'=>'required',
            'email'=>'required',
            'contactnumber'=>'required',
            'gender'=>'required',
            'role_id'=>'required',
            'full_time'=>'required',

        ]);

        $employee->name = $request->name;
		$employee->slug = str_slug($request->name);
		$employee->address = $request->address;
        $employee->email = $request->email;
        $employee->contactnumber = $request->contactnumber;
        $employee->gender = $request->gender;
		$employee->full_time = $request->full_time;
		$employee->role_id  = $request->role_id;		
		$employee->save();
		
		$request->session()->flash('status', 'New Employee created');
		return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();

        Session::flash('success','Employee deleted');
        return redirect()->route('employee.index');

    }
}
