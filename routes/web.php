<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Input;
use App\Employee;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::get('/admin', 'AdminController@admin')    
    ->middleware(['is_admin'])    
    ->name('admin');

Route::resource('employee','EmployeeController');
Route::resource('department','DepartmentController');
Route::resource('role','RoleController');
// Route::resource('payroll','PayrollController');
Route::get('/employee/payroll/{id}', 'PayrollController@payrollIndex')->name('payrolls.show');
Route::get('/payrolls/create/{id}', 'PayrollController@create')->name('payrolls.create');
Route::post('/payrolls/{id}', 'PayrollController@store')->name('payrolls.store');
Route::get('/employee/payroll/{id}/edit', 'PayrollController@edit')->name('payrolls.edit');
Route::patch('/payrolls/update/{id}', 'PayrollController@update')->name('payrolls.update');

Route::delete('/payrolls/delete/{id}', 'PayrollController@destroy')->name('payrolls.destroy');



Route::any('/search',function(){
    $q = Input::get ( 'q' );
    $employee = Employee::where('name','LIKE','%'.$q.'%')->orWhere('email','LIKE','%'.$q.'%')->get();
    if(count($employee) > 0)
        return view('employee.index')->withDetails($employee)->withQuery ( $q );
    else return view ('employee.index')->withMessage('No Details found. Try to search again !');
});
